<?php

/**
 * @file
 * Provides a Rules action for granting userpoints based of the quantity for a certain product type in an order
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_userpoints_grant_rules_action_info() {
  return array(
    'commerce_userpoints_grant_rules_action_grant_points' => array(
      'label' => t('Grant !points to a user based on quantity of items in an order.', userpoints_translation()),
      'named parameter' => TRUE,
      'parameter' => array(
        //commerce stuff
        'commerce_order' => array(
          'type' => 'commerce_order',
          'label' => t('Order'),
          'description' => t('The order which should be used to calculate the totally quantity'),
        ),
        'commerce_product_type' => array(
          'label' => t('Product type within the order to count quantity of'),
          'type' => 'text',
          'options list' => 'commerce_userpoints_grant_product_types',
          'optional' => TRUE,
          'multiple' => TRUE,
        ),

        //userpoints stuff
        'user' => array(
          'type' => 'user',
          'label' => t('User'),
          'description' => t('The user that will receive the !points', userpoints_translation()),
        ),
        'tid' => array(
          'label' => t('!Points category', userpoints_translation()),
          'type' => 'integer',
          'options list' => 'commerce_userpoints_grant_get_categories',
        ),
        'description' => array(
          'label' => t('Description'),
          'type' => 'text',
          'description' => t('Can contain the reason why the points have been granted.'),
          'restriction' => 'input',
          'optional' => TRUE,
        ),
        'reference' => array(
          'label' => t('Reference'),
          'type' => 'text',
          'description' => t('Can contain a reference for this transaction.'),
          'optional' => TRUE,
        ),
        'display' => array(
          'label' => t('Display'),
          'type' => 'boolean',
          'description' => t('Whether or not to show a message to the user when this !points transaction is added. You may want to turn this off and provide your own message in another Rules action.', userpoints_translation()),
          'default value' => TRUE,
        ),
        'moderate' => array(
          'label' => t('Moderate'),
          'type' => 'text',
          'description' => t('Whether or not this !points transaction should be moderated.', userpoints_translation()),
          'options list' => 'commerce_userpoints_grant_moderate',
        ),
        'expirydate' => array(
          'label' => t('Expiration Date'),
          'type' => 'date',
          'description' => t('Define when the !points should expire.', userpoints_translation()),
          'optional' => TRUE,
        ),
      ),
      'group' => t('!Points', userpoints_translation()),
      'provides' => array(
        'commerce_userpoints_granted' => array(
          'type' => 'integer',
          'label' => t('!Points granted', userpoints_translation()),
        ),
        'commerce_userpoints_total' => array(
          'type' => 'integer',
          'label' => t('New !Points total', userpoints_translation()),
        ),
      ),
    ),
  );
}

/**
 * Wrapper function for userpoints_get_categories().
 *
 * Rules.module uses different arguments for option list callbacks than
 * userpoints_get_categories expects.
 */
function commerce_userpoints_grant_get_categories() {
  return userpoints_get_categories();
}

/**
 * Simple callback that lists the categories including an option for all.
 */
function commerce_userpoints_grant_get_all_categories() {
  return array('all' => t('All categories')) + userpoints_get_categories();
}

/**
 * Simple callback that lists the possible moderate values.
 */
function commerce_userpoints_grant_moderate() {
  return array(
    'default' => t('Use the site default'),
    'approved' => t('Automatically approved'),
    'moderated' => t('Added to moderation'),
  );
}

/**
 * Return list of product types available
 */
function commerce_userpoints_grant_product_types() {
  $types = array();
  $types['all'] = t('All product types');
  foreach (commerce_product_types() as $type => $line_item_type) {
    $types[$type] = $line_item_type['name'];
  }

  return $types;
}

/**
 * Rules action - grant points to a user.
 */
function commerce_userpoints_grant_rules_action_grant_points($params) {
  // The passed in $entity is the unwrapped object. To get type and id, we need
  // the wrapped version of it.
  $state = $params['state'];
  $uid = is_object($params['user']->uid) ? $params['user']->getIdentifier() : $params['user']->uid;
  $order = $state->currentArguments['commerce_order'];
  $entity = $order;
  $ptype = $state->currentArguments['commerce_product_type'];
  $quantity = 0;
  $new_total = NULL;

  // If we received an order, get the total quantity of products on it.
  if (!empty($order)) {
    $wrapper = entity_metadata_wrapper('commerce_order', $order);

    if (!empty($wrapper->commerce_line_items)) {
      $line_items = $wrapper->commerce_line_items;
      foreach ($line_items as $line_item) {
        if (!$line_item instanceof EntityMetadataWrapper) {
          $line_item = entity_metadata_wrapper('commerce_line_item', $line_item);
        }

        if ($line_item->commerce_product->value()->type == $ptype || $ptype == 'all') {
          $quantity += $line_item->quantity->value();
        }
      }
    }
  }

  if (!empty($quantity)) {
    $params['points'] = $quantity;

    // Map $moderate value to the actual value used by the API.
    $moderate_mapping = array(
      'default' => NULL,
      'approved' => FALSE,
      'moderated' => TRUE,
    );
    $params = array(
      // User id might be a int or a EntityValueWrapper.
      'uid' => $uid,
      'entity_type' => $entity ? $entity->type() : NULL,
      'entity_id' => $entity ? $entity->getIdentifier() : NULL,
      'moderate' => $moderate_mapping[$params['moderate']],
      // Rules defaults to FALSE if the date format can not be parsed.
      // Use NULL instead since FALSE means no expiration.
      'expirydate' => $params['expirydate'] ? $params['expirydate'] : NULL,
      'operation' => 'Insert',
    ) + $params;

    unset($params['state']);
    unset($params['user']);
    unset($params['entity']);
    unset($params['settings']);
    unset($params['commerce_order']);
    userpoints_userpointsapi($params);

    //get new total
    $new_total = userpoints_get_current_points($uid, $params['tid']);
  }

  //return the quantity so it can be used in other actions
  return array('commerce_userpoints_granted' => $quantity, 'commerce_userpoints_total' => $new_total);
}

/**
 * Not sure this is needed
 * @todo - remove it?
 */
function commerce_userpoints_grant_rules_action_grant_points_form_alter(&$form, &$form_state) {
  // Empty value by default.
  $form['parameter']['expirydate']['settings']['expirydate']['#default_value'] = '';
}
